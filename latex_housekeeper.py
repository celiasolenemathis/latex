import os


class Housekeeper:
    def __init__(self, root_dir=""):
        self.illegal_file_endings = tuple([".aux", ".fdb_latexmk", ".fls", ".log", ".gz", ".auxlock", ".toc", ".out", ".lof", ".lot", ".nav", ".snm"])
        self.num_of_deleted_files = 0
        self.root_dir = root_dir

    @classmethod
    def use_file_dir(cls):
        return cls(os.path.dirname(os.path.realpath(__file__)))

    def cleanup(self):
        for walk_root, walk_dirs, walk_files in os.walk(self.root_dir, topdown=False):
            for file_name in walk_files:
                self.__handle_file(walk_root, file_name)

    def __handle_file(self, walk_root, file_name):
        if file_name.endswith(self.illegal_file_endings):
            os.remove(os.path.join(walk_root, file_name))
            self.num_of_deleted_files += 1


response_use_default_dir = input("Do you ant to cleanup the dir where housekeeper is located? (y/n) ")
housekeeper = None

if response_use_default_dir.lower() == "y":
    housekeeper = Housekeeper.use_file_dir()
elif response_use_default_dir.lower() == "n":
    response_custom_dir = input("Please define your custom dir: ")
    housekeeper = Housekeeper(response_custom_dir)

if housekeeper is not None:
    housekeeper.cleanup()
    print("Cleaned up {} files".format(housekeeper.num_of_deleted_files))
