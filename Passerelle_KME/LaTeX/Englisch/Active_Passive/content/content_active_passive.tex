% ---IMPORTANT---
% This content is designed for a landscape orientation
% ---------------

\section{Active and passive voice}
\subsection{What are they?}
The defining difference in the active and passive voices are which parts of the sentence act upon each other. 
\textbf{Active voice} has a subject act upon its verb. \textbf{Passive voice} has a verb that acts upon its subject. \\
Using the active voice conveys a stronger, clearer tone, while the passive voice leads to a more subtler sentence. However the passive voice should be used sparingly. Overusing it can lead to an unwieldy text, that may look a bit dated. 

\subsection{Tenses in active and passive voice}
\begin{center}
        \renewcommand{\arraystretch}{1.5}
        \begin{tabular} {|l|l|l|l|}
            \hline
            \textbf{Tense} & \textbf{Form: be (same tense) + partici.} & \textbf{Active} & \textbf{Passive} \\
            \hline
            Present simple & to be + \underline{participle} & I \textcolor{mred}{ask} him. & He \textcolor{mred}{is} \underline{asked} by me.\\
            \hline
            Present continuous & to be (cont.) + \underline{participle} & I \textcolor{mred}{am asking} him. & He \textcolor{mred}{is being} \underline{asked} by me.\\
            \hline
            Past simple & to be (past sim.) + \underline{participle} & I \textcolor{mred}{asked} him. & He \textcolor{mred}{was} \underline{asked} by me.\\
            \hline
            Past continuous & to be (past cont.) + \underline{participle} & I \textcolor{mred}{was asking} him. & He \textcolor{mred}{was being} \underline{asked} by me.\\
            \hline
            Present perfect simple & \multirow{2}{*}{to be (Pres. perf. sim.) + \underline{participle}} & I \textcolor{mred}{have asked} him. & \multirow{2}{*}{He \textcolor{mred}{has been} \underline{asked} by me.}\\
            \cline{1-1} \cline{3-3}
            Present perfect continuous &  & I \textcolor{mred}{have been} asking him. & \\
            \hline
            Past perfect simple & \multirow{2}{*}{to be (Past. perf. sim.) + \underline{participle}} & I \textcolor{mred}{had asked} him. & \multirow{2}{*}{He \textcolor{mred}{had been} \underline{asked} by me.} \\
            \cline{1-1} \cline{3-3}
            Past perfect continuous &  & I \textcolor{mred}{had been} asking him. & \\
            \hline
            Future 1 & will + be + \underline{participle} & I \textcolor{mred}{will} ask him. & He \textcolor{mred}{will be} \underline{asked} by me. \\
            \hline
            Future 2 & will + to be (Pres. perf. sim) + \underline{participle} & I \textcolor{mred}{will have} asked him. & He \textcolor{mred}{will have been} \underline{asked} by me. \\
            \hline 
            Conditional 1 & con.verb + be + \underline{participle} & I \textcolor{mred}{would} ask him. & He \textcolor{mred}{would be} \underline{asked} by me. \\
            \hline
            Conditional 2 & con.verb + to be (Pres. perf. sim.) + \underline{participle} & I \textcolor{mred}{would} have asked him. & He \textcolor{mred}{would have been} \underline{asked} by me. \\
            \hline
        \end{tabular}
\end{center}

\newpage

\subsection{Transforming active into passive}
When transforming active voice into passive voice, it is important to note, that the subject of the active sentence becomes the object of the passive sentence. Depending on the construction of the passive sentence the subject (of the active sentence) may become obsolete.
\begin{flushleft}
        \renewcommand{\arraystretch}{1.5}
        \begin{tabular} {p{1.5cm} p{1.2cm} p{1.8cm} p{5.6cm} p{5cm}}
            \textbf{Active:} & \colorbox{lightblue}{Subject} & \colorbox{lightorange}{Object} \qquad & \colorbox{lightblue}{Lynn} sees \colorbox{lightorange}{Marc} every day. & \colorbox{lightblue}{They} kick \colorbox{lightorange}{the ball}. \\
            \textbf{Passive:} & \colorbox{lightorange}{Object} & \colorbox{lightblue}{Subject} \qquad & \colorbox{lightblue}{Marc} is seen \textcolor{mred}{\underline{by}} \colorbox{lightorange}{Lynn} every day. & \colorbox{lightblue}{The Ball} is kicked \textcolor{mred}{\underline{by}} \colorbox{lightorange}{them}. \\
        \end{tabular}
\end{flushleft}

\begin{flushleft}
        \renewcommand{\arraystretch}{1.5}
        \begin{tabular}{p{4.8cm} p{6cm} p{7cm}}
            \textbf{Subject becomes obsolete:} & \colorbox{lightblue}{They} do not sell \colorbox{lightorange}{cakes} in bookshops. & \colorbox{lightblue}{Cakes} are not sold in bookshops (\colorbox{lightorange}{\cancel{by them}}). \\
        \end{tabular}
\end{flushleft}

\subsubsection*{Two objects}
If a sentence contains two objects, it is possible construct two different (correct) passive sentences.

\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular}{p{5.5cm} l}
        \multirow{2}{*}{\colorbox{lightblue}{John} gave \colorbox{lightorange}{Rita} \colorbox{lightorange}{a present}} & 1. \colorbox{lightblue}{Rita} was given \colorbox{lightorange}{a present} by \colorbox{lightorange}{John}. \qquad \\
        & 2. \colorbox{lightblue}{A present} was given to \colorbox{lightorange}{Rita} by \colorbox{lightorange}{John}.
    \end{tabular}
\end{flushleft}

\subsubsection*{Prepositions}
If a preposition follows a verb, it carries over into the passive sentence. It remains immediately \underline{after the verb}.
\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {p{5.5cm} l}
        \colorbox{lightblue}{Someone} broke \textcolor{mred}{\underline{into}} \colorbox{lightorange}{our shop}. & \colorbox{lightorange}{Our shop} was broken \textcolor{mred}{\underline{into}} (\colorbox{lightorange}{\cancel{by someone}}). \\
        \colorbox{lightblue}{They} cared \textcolor{mred}{\underline{for}} \colorbox{lightorange}{the baby}. & \colorbox{lightblue}{The baby} was cared \textcolor{mred}{\underline{for}} (\colorbox{lightorange}{\cancel{by them}}).
    \end{tabular}
\end{flushleft}

\begin{multicols}{2}
    \subsubsection*{Combining passive with infinitives}
The infinitive passive voice is used after \textcolor{mred}{modal verbs:}
\begin{itemize}
    \item You \textcolor{mred}{have to} be tested on your English grammar.
    \item Karen \textcolor{mred}{might} be angry.
\end{itemize}
Modal verbs are: can/could, may/might, shall/should, will/would and must.

\subsection{When to use the passive voice}
\begin{itemize}
    \item If it is not known who does/did something.
    \item If it is obvious who does/did something.
    \item If it is not important who does/did something.
    \item To emphasise new information at the end of a sentence.
    \item To create a formal style.
\end{itemize}
\end{multicols}

