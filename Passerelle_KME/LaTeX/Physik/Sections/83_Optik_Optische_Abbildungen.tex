\clearpage % flushes out all floats
\subsection{Optische Abbildungen}
\subsubsection{Abbildungen an einer Lochblende: Reelle Bilder}

Eine direkte Anwendung der geradlinigen Ausbreitung des Lichts ist die Lochkamera. Eine Lochkamera ist das einfachste Gerät, mit dem optische Abbildungen erzeugt werden können. Im Gegensatz zum Fotoapparat benötigt 
    sie keine optische Linse zur Abbildung eines Gegenstandes, sondern nur eine dunkle, lichtdichte Zelle (eine \glqq camera obscura\grqq{}), mit einer kleinen, verschiebbaren Öffnung in dieser Zelle. Das auf der gegenüberliegenden Innenseite entstehende Bild lässt sich auf einem halb transparenten Schirm festhalten. Das optische Bild, das mit einer Lochkamera erzeugt wird, kann mit einer Mattscheibe aufgefangen werden. Solche Bilder heissen reelle (wirkliche) Bilder.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
            
            % Filled areas
            \draw[fill=MyRed!30,draw=none] (1,0) -- (1,1.5) -- (4.7,0) -- cycle;
            \draw[fill=MyRed!30,draw=none] (4.7,0) -- (9,0) -- (9,-1.75) -- cycle;
            
            % Arrows
            %% Gegenstand & Bild
            \draw[draw=none,fill=MyBlack!75] (0.95,0) -- (0.95,1.15) -- (0.85,1.15) -- (1,1.5) -- (1.15,1.15) -- (1.05,1.15) -- (1.05,0) -- cycle; % G
            \draw[draw=none,fill=MyBlack!75] (8.95,0) -- (8.95,-1.4) -- (8.85,-1.4) -- (9,-1.75) -- (9.15,-1.4) -- (9.05,-1.4) -- (9.05,0) -- cycle; % B
            
            % Dash dotted line
            \draw[thick,dashdotted,MyBlack] (0,0) -- (10,0);
            
            % Light ray
            \draw[->,thick,MyRed] (1,1.5) -- (9,-1.75);

            % Hole/Face plate
            \draw[draw=none,fill=MyBlack!75!white] (4.6,-2) rectangle (4.8,-0.2);
            \draw[draw=none,fill=MyBlack!75!white] (4.6,0.2) rectangle (4.8,2);
            \draw[thick,MyBlack] (4.7,-2) -- (4.7,-2.5);

            % Lengths
            %% G
            \draw[thick,MyBlack] (1,1.5) -- (0,1.5);
            \draw[<->,thick,MyBlack] (0.5,0) -- (0.5,1.5) node[pos=0.5, anchor=east, shift={(0mm,0mm)}, text=MyBlack] {$G$};
            %% B
            \draw[thick,MyBlack] (9,-1.75) -- (10,-1.75);
            \draw[<->,thick,MyBlack] (9.5,0) -- (9.5,-1.75) node[pos=0.5, anchor=west, shift={(0mm,0mm)}, text=MyBlack] {$B$};
            %% g
            \draw[thick,MyBlack] (1,0) -- (1,-2.5);
            \draw[<->,thick,MyBlack] (1,-2.25) -- (4.7,-2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$g$};
            %% b
            \draw[thick,MyBlack] (9,-1.75) -- (9,-2.5);
            \draw[<->,thick,MyBlack] (4.7,-2.25) -- (9,-2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$b$};

            % Legend
            \draw[draw=MyBlack,fill=MyLightGrey!50] (6,0.5) rectangle (10,2.5) node[pos=0.5, shift={(0mm,0mm)}, text=MyBlack, align=left] {$G$: Gegenstandsgrösse\\$B$: Bildgrösse\\$g$: Gegenstandsweite\\$b$: Bildweite};

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Abbildung an einer Lochkamera.}
\end{figure}

Der \textbf{Abbildungsmassstab} \textit{v} gibt das Verhältnis zwischen der Grösse des reellen Bildes \textit{B} und derjenigen des Gegenstandes \textit{G}. Er wird wie folgt berechnet:
    \begin{equation}
        v = \frac{B}{G} = \frac{b}{g} \label{eq:Abbildungsmassstab}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Baum habe eine Höhe von $18\si{\metre}$ und sei $25\si{\metre}$ vom Loch der Kamera entfernt. \\\hspace*{\fill}

\textit{Gesucht:} Wie muss die Entfernung \textit{b} zwischen Loch und Mattscheibe (Höhe $h = 20\si{\cm}$) gewählt werden, damit der Baum die Mattscheibe in der ganzen Höhe ausfüllt? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \frac{B}{G} = \frac{b}{g} \ \Rightarrow \ b = \frac{B \cdot g}{G} = \frac{0.2\si{\metre} \cdot 25\si{\metre}}{18\si{\metre}} = \underline{\underline{0.278\si{\metre}}}
\]

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Schlüsselloch wirft auf der drei Meter entfernten, gegenüberliegenden Wand ein Bild der Sonne mit einem Durchmesser von $30\si{\mm}$. \\\hspace*{\fill}

\textit{Gesucht:} Berechnen Sie die Entfernung Erde-Sonne, wenn man weiss, dass der Sonnendurchmesser ca. 109mal so gross wie der Erddurchmesser ($r_{Erde} = 6'370\si{\km}$) ist. \\\hspace*{\fill}

\textit{Lösung:}
\[
    \frac{B}{G} = \frac{b}{g} \ \Rightarrow \ \frac{b \cdot G}{B} = \frac{3\si{\metre} \cdot 2 \cdot 109 \cdot 6'370 \cdot 10^3\si{\metre}}{0.03\si{\metre}} = \underline{\underline{1.39 \cdot 10^{11}\si{\metre}}}
\]

\subsubsection{Linsen und Linsenabbildungen}

Bei optischen Linsen gibt es grundsätzlich zwei Arten: \textit{Sammellinsen (konvexe Linsen)} und \textit{Streulinsen (konkave Linsen)}. Sammellinsen sind in der Mitte dicker, Zerstreuungslinsen sind in der Mitte 
    dünner als am Rand. \\\hspace*{\fill}

Die Bezeichnungen \glqq Sammel-\grqq{} bzw. \glqq Zerstreuungslinsen\grqq{} entsprechen den optischen Eigenschaften:
    \begin{itemize}
        \item Fällt ein Parallelstrahlenbündel auf eine Sammellinse, so läuft es hinter der Linse (mehr oder weniger genau) in einem Punkt, dem Brennpunkt \textit{F} zusammen.
        \item Fällt ein Parallelstrahlenbündel auf eine Zerstreuungslinse, so läuft es hinter der Linse auseinander.
    \end{itemize}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
            
            % Dash dotted line
            \draw[thick,dashdotted,MyBlack] (1,0) -- (9,0);

            % Prisms
            %% Upper part
            \draw[draw=MyGreen,fill=MyGreen!60] (4,0.05) -- (4.05,0.5) -- (5.95,0.5) -- (6,0.05) -- cycle; % bottom
            \draw[draw=MyGreen,fill=MyGreen!60] (4.075,0.6) -- (4.175,1.05) -- (5.825,1.05) -- (5.925,0.6) -- cycle; % 2nd from bottom
            \draw[draw=MyGreen,fill=MyGreen!60] (4.2,1.15) -- (4.4,1.6) -- (5.6,1.6) -- (5.8,1.15) -- cycle; % 3rd from bottom
            \draw[draw=MyGreen,fill=MyGreen!60] (4.425,1.7) -- (4.7,2.15) -- (5.3,2.15) -- (5.575,1.7) -- cycle; % 4th from bottom
            \draw[draw=MyBlack,fill=MyGrey!60] (4.725,2.25) -- (5,2.7) -- (5.275,2.25) -- cycle node[anchor=west, shift={(7mm,2mm)}, text=MyBlack] {\glqq Prisma\grqq}; % top
            %% Lower part
            \draw[draw=MyGreen,fill=MyGreen!60] (4,-0.05) -- (4.05,-0.5) -- (5.95,-0.5) -- (6,-0.05) -- cycle; % top
            \draw[draw=MyGreen,fill=MyGreen!60] (4.075,-0.6) -- (4.175,-1.05) -- (5.825,-1.05) -- (5.925,-0.6) -- cycle; % 2nd from top
            \draw[draw=MyGreen,fill=MyGreen!60] (4.2,-1.15) -- (4.4,-1.6) -- (5.6,-1.6) -- (5.8,-1.15) -- cycle; % 3rd from top
            \draw[draw=MyGreen,fill=MyGreen!60] (4.425,-1.7) -- (4.7,-2.15) -- (5.3,-2.15) -- (5.575,-1.7) -- cycle; % 4th from top
            \draw[draw=MyGreen,fill=MyGreen!60] (4.725,-2.25) -- (5,-2.7) -- (5.275,-2.25) -- cycle; % bottom
            
            % Light rays
            \foreach \y in {-2.475,-1.925,...,2.925}{
                \draw[->,thick,MyRed] (1,\y) -- (2.5,\y);
                }
            %% Top part
            \draw[thick,MyRed] (2.6,2.475) -- (4.8625,2.475) -- (5.18,2.4) -- (9,-0.85); % top
            \draw[thick,MyRed] (2.6,1.925) -- (4.5625,1.925) -- (5.4625,1.875) -- (9,-0.7389); % 4th from top
            \draw[thick,MyRed] (2.6,1.375) -- (4.3,1.375) -- (5.72,1.325) -- (9,-0.58); % 3rd from top
            \draw[thick,MyRed] (2.6,0.825) -- (4.125,0.825) -- (5.875,0.775) -- (9,-0.3647); % 2nd from top
            \draw[thick,MyRed] (2.6,0.275) -- (4.025,0.275) -- (5.95,0.25) -- (9,-0.1219); % bottom
            %% Lower part
            \draw[thick,MyRed] (2.6,-2.475) -- (4.8625,-2.475) -- (5.18,-2.4) -- (9,0.85); % bottom
            \draw[thick,MyRed] (2.6,-1.925) -- (4.5625,-1.925) -- (5.4625,-1.875) -- (9,0.7389); % 4th from top
            \draw[thick,MyRed] (2.6,-1.375) -- (4.3,-1.375) -- (5.72,-1.325) -- (9,0.58); % 3rd from top
            \draw[thick,MyRed] (2.6,-0.825) -- (4.125,-0.825) -- (5.875,-0.775) -- (9,0.3647); % 2nd from top
            \draw[thick,MyRed] (2.6,-0.275) -- (4.025,-0.275) -- (5.95,-0.25) -- (9,0.1219); % top
            
            \draw[draw=MyRed,fill=white,thick] (8,0) circle (0.075) node[above, shift={(0mm,2mm)}, text=MyBlack] {$F$};
                
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Sammellinse.}
\end{figure}

Der Abstand des Brennpunkts vom optischen Mittelpunkt (Zentrum der Linse) heisst Brennweite \textit{f} der Linse. Für Brillen ist auch die sogenannte \textit{Brechkraft D} mit der Einheit Dioptrie (dpt) gebräuchlich. 
    Sie wird folgendermassen berechnet:
    \begin{equation}
        D = \frac{1}{f} \ \left[\frac{1}{\si{\metre}}\right] = [1\si{\metre}^{-1}] = [1\textnormal{dpt}] \label{eq:Brechkraft}
    \end{equation}

Bei kurzsichtigen Menschen ist der Brennpunkt \textit{F} vor der Netzhaut, d.h. sie benötigen eine Streulinse; bei weitsichtigen Menschen ist der Brennpunkt \textit{F} hinter der Netzhaut, weswegen sie eine 
    Sammellinse brauchen.
    \newpage

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
            
            % Filled areas
            \draw[fill=MyGrey!30,draw=none] (1,0) -- (1,1.5) -- (5.705,0) -- cycle;
            \draw[fill=MyGrey!30,draw=none] (5.705,0) -- (9,0) -- (9,-1.05) -- cycle;
            \draw[fill=MyRed!30,draw=none,fill opacity=0.5] (1,1.5) -- (5.705,1.5) -- (9,-1.05) -- (5.705,-1.05) -- cycle;
            
            % Arrows
            %% Gegenstand & Bild
            \draw[draw=none,fill=MyBlack!75] (0.95,0) -- (0.95,1.15) -- (0.85,1.15) -- (1,1.5) -- (1.15,1.15) -- (1.05,1.15) -- (1.05,0) -- cycle; % G
            \draw[draw=none,fill=MyBlack!75] (8.95,0) -- (8.95,-0.7) -- (8.85,-0.7) -- (9,-1.05) -- (9.15,-0.7) -- (9.05,-0.7) -- (9.05,0) -- cycle; % B

            % Lens
            \draw[thick,draw=MyGreen,fill=MyGreen!60] (5.705,2) arc[start angle=150,delta angle=60,radius=4] arc[start angle=-30,delta angle=60,radius=4] -- cycle;
            \draw[thick,white] (5.705,2.05) -- (5.705,-2.05);
            
            % Dash dotted line
            \draw[thick,dashdotted,MyBlack] (0,0) -- (10,0);
            
            % Lengths
            %% G
            \draw[thick,MyBlack] (1,1.5) -- (0,1.5);
            \draw[<->,thick,MyBlack] (0.5,0) -- (0.5,1.5) node[pos=0.5, anchor=east, shift={(0mm,0mm)}, text=MyBlack] {$G$};
            %% B
            \draw[thick,MyBlack] (9,-1.05) -- (10,-1.05);
            \draw[<->,thick,MyBlack] (9.5,0) -- (9.5,-1.05) node[pos=0.5, anchor=west, shift={(0mm,0mm)}, text=MyBlack] {$B$};
            %% g
            \draw[thick,MyBlack] (1,-0.75) -- (1,-2.5);
            \draw[thick,MyBlack] (5.705,-2) -- (5.705,-2.5);
            \draw[<->,thick,MyBlack] (1,-2.25) -- (5.705,-2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$g$};
            %% b
            \draw[thick,MyBlack] (9,-1.75) -- (9,-2.5);
            \draw[<->,thick,MyBlack] (5.705,-2.25) -- (9,-2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$b$};
            %% f
            \draw[thick,MyBlack] (3.768,0.25) -- (3.768,2.5);
            \draw[thick,MyBlack] (5.705,2) -- (5.705,2.5);
            \draw[thick,MyBlack] (7.64,0.75) -- (7.64,2.5);
            \draw[<->,thick,MyBlack] (3.768,2.25) -- (5.705,2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f$};
            \draw[<->,thick,MyBlack] (5.705,2.25) -- (7.64,2.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f$};
            
            % Light rays
            \draw[->,thick,MyRed] (1,1.5) -- (9,-1.05);
            \draw[->,thick,MyRed] (1,1.5) -- (5.705,-1.05) -- (9,-1.05);
            \draw[->,thick,MyRed] (1,1.5) -- (5.705,1.5) -- (9,-1.05);

            % Points
            \draw[draw=MyBlack,fill=white] (1,1.5) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$P$};
            \draw[draw=MyBlack,fill=white] (1,0) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$Q$};
            \draw[draw=MyBlack,fill=white] (9,-1.05) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$U$};
            \draw[draw=MyBlack,fill=white] (9,0) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$V$};
            \draw[draw=MyBlack,fill=white] (5.705,0) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$X$};

            \draw[draw=MyRed,fill=white] (3.768,0) circle (0.05) node[anchor=north, shift={(0mm,-1mm)}, text=MyBlack] {$F_1$};
            \draw[draw=MyRed,fill=white] (7.64,0) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$F_2$};
               
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Bildkonstruktion an der Sammellinse: reelles Bild.}
    \label{fig:Bildkonstruktion an der Sammellinse reelles Bild}
\end{figure}

Zur geometrischen Konstruktion des optischen Bildes eines Gegenstandes (Abbildung \ref{fig:Bildkonstruktion an der Sammellinse reelles Bild}, Punkt \textit{P}) benutzen wir einen Parallel-, einen Brenn- und einen 
    Mittelpunktstrahl. Das optische Bild \textit{U} entsteht im Schnittpunkt der entsprechenden an der Linse gebrochenen Strahlen. Aus Abbildung \ref{fig:Bildkonstruktion an der Sammellinse reelles Bild} lesen wir für den Abbildungsmassstab \textit{v} (ähnliche Dreiecke \textit{PQX} und \textit{UVX}):
    \[
        v = \frac{B}{G} = \frac{\overline{UV}}{\overline{PQ}} = \frac{b}{g}
    \]

Daraus ergibt sich eine Formel, mit der man die Brennweite \textit{f} und die Abstände des betrachteten Objektes berechnen kann -- die sogenannte \textbf{Linsengleichung}:
    \begin{equation}
        \frac{1}{f} = \frac{1}{b} + \frac{1}{g} \label{eq:Linsengleichung}
    \end{equation}

Liegt der Gegenstand \textit{G} zwischen der Linse und einem Brennpunkt, so verlaufen die von \textit{P} ausgehenden Strahlen nach der Brechung an der Linse auseinander (sie divergieren) und erzeugen deshalb 
    \textbf{kein reelles Bild}. Stattdessen wird ein \textit{virtuelles Bild} erzeugt.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
            
            % Filled areas
            \draw[fill=MyGrey,draw=none] (5.75,0) -- (5.75,1.05) -- (6.69,0) -- cycle;
            \draw[fill=MyGrey!30,draw=none] (4,0) -- (5.75,0) -- (5.75,1.05) -- (4,3) -- cycle;
            \draw[fill=MyRed!50,draw=none,path fading=west] (5.75,1.05) -- (4,3) -- (1.5,3) -- (1.5,-1.5) -- cycle;
            \draw[fill=MyRed!50,draw=none,path fading=west] (9,3) -- (5.75,3) -- (5.75,1.05) -- cycle;
            
            % Arrows
            %% Gegenstand & Bild
            \draw[draw=none,fill=MyBlack!75] (8.95,0) -- (8.95,2.65) -- (8.85,2.65) -- (9,3) -- (9.15,2.65) -- (9.05,2.65) -- (9.05,0) -- cycle; % B
            \draw[draw=none,fill=MyBlack!75] (5.7,0) -- (5.7,0.7) -- (5.6,0.7) -- (5.75,1.05) -- (5.9,0.7) -- (5.8,0.7) -- (5.8,0) -- cycle node[anchor=east, shift={(0mm,4mm)}, text=MyBlack] {$G$}; % G

            % Lens
            \draw[thick,draw=MyGreen,fill=MyGreen!60] (4,2) arc[start angle=150,delta angle=60,radius=4] arc[start angle=-30,delta angle=60,radius=4] -- cycle;
            \draw[thick,white] (4,2.05) -- (4,-2.05);
            
            % Dash dotted line
            \draw[thick,dashdotted,MyBlack] (0,0) -- (10,0);
            
            % Lengths
            \draw[thick,dashed,MyBlack] (4,2) -- (4,3);
            %% B
            \draw[thick,MyBlack] (9,3) -- (10,3);
            \draw[<->,thick,MyBlack] (9.5,3) -- (9.5,0) node[pos=0.5, anchor=west, shift={(0mm,0mm)}, text=MyBlack] {$B$};
            %% g
            \draw[thick,dashed,MyBlack] (4,-1) -- (4,-3);
            \draw[thick,MyBlack] (5.75,-0.5) -- (5.75,-1.5);
            \draw[<->,thick,MyBlack] (4,-1.25) -- (5.75,-1.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$g$};
            %% b
            \draw[thick,MyBlack] (9,-0.5) -- (9,-3);
            \draw[<->,thick,MyBlack] (4,-2.75) -- (9,-2.75) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$b$ (neg.)};
            %% f
            \draw[thick,MyBlack] (6.69,-0.25) -- (6.69,-2.25);
            \draw[<->,thick,MyBlack] (4,-2) -- (6.69,-2) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f$};
            
            % Light rays
            \draw[thick,MyRed,dashed] (9,3) -- (4,0);
            \draw[thick,MyRed,dashed] (9,3) -- (3.5,3);
            \draw[thick,MyRed,dashed] (4,3) -- (7.589,-1);
            \draw[thick,MyRed,dashed] (9,3) -- (4,1.05);
            \draw[->,thick,MyRed] (5.75,1.05) -- (4,3) -- (0,3);
            \draw[->,thick,MyRed] (5.75,1.05) -- (4,1.05) -- (0,-0.51);
            \draw[->,thick,MyRed] (5.75,1.05) -- (4,0) -- (0,-2.4);

            % Points
            \draw[draw=MyBlack,fill=white] (5.75,1.05) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$P$};
            \draw[draw=MyBlack,fill=white] (5.75,0) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$Q$};
            \draw[draw=MyBlack,fill=white] (9,3) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$U$};
            \draw[draw=MyBlack,fill=white] (4,3) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$Y$};
            \draw[draw=MyBlack,fill=white] (9,0) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$V$};
            \draw[draw=MyBlack,fill=white] (4,0) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$X$};

            \draw[draw=MyRed,fill=white] (1.31,0) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$F_1$};
            \draw[draw=MyRed,fill=white] (6.69,0) circle (0.05) node[anchor=south, shift={(0mm,1mm)}, text=MyBlack] {$F_2$};
               
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Bildkonstruktion an der Sammellinse: virtuelles Bild.}
    \label{fig:Bildkonstruktion an der Sammellinse virtuelles Bild}
\end{figure}

Verlängert man diese Strahlen aber rückwärts, so schneiden sie sich in einem Punkt \textit{U} und erzeugen dort ein virtuelles Bild \textit{B}. Die Abbildungsgleichungen $v = \frac{B}{G} = \frac{b}{g}$ und 
    $\frac{b}{g} = \frac{f}{g - f}$ gelten auch für das virtuelle Bild (Abbildung \ref{fig:Bildkonstruktion an der Sammellinse virtuelles Bild}, ähnliche Dreiecke \textit{PQX} und \textit{UVX} bzw. \textit{PG$F_2$} und \textit{YX$F_2$}). Die Bildweite \textit{b}, die Bildgrösse \textit{B} und der Abbildungsmassstab \textit{v} werden jetzt \textbf{negativ}, weil der Nenner ($g - f$) negativ ist.
    \newpage

Wenn Licht auf Streulinsen fällt, dann wird das austretende Licht nicht gebündelt, sondern es divergiert, d.h. es geht auseinander. Dennoch ist es möglich, auch bei Streulinsen einen Brennpunkt und eine Brennweite 
    zu finden. Treffen die Lichtstrahlen parallel zur optischen Achse auf die Streulinse auf, so müssen die gebrochenen Lichtstrahlen zurück verlängert werden, was dann den Brennpunkt und die Brennweite ergibt.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
            
            % Lens
            \draw[thick,draw=MyGreen,fill=MyGreen!60] (5.75,2) arc[start angle=150,delta angle=60,radius=4] -- (4.25,-2)  arc[start angle=-30,delta angle=60,radius=4] -- cycle;
            
            % Dash dotted line
            \draw[thick,dashdotted,MyBlack] (1,0) -- (7.779,0);

            % Light rays
            \foreach \y in {1.75,1.25,...,-1.75}{
                \draw[->,thick,MyRed] (1,\y) -- (2.5,\y);
                }
            %% Top
            \draw[->,thick,MyRed] (2.6,1.75) -- (4.38,1.75) -- (5.7,1.9) -- (7.779,2.9); % top
            \draw[->,thick,MyRed] (2.6,1.25) -- (4.6,1.25) -- (5.45,1.35) -- (7.779,2.1998); % 2nd from top
            \draw[->,thick,MyRed] (2.6,0.75) -- (4.7,0.75) -- (5.3,0.817) -- (7.779,1.3875); % 3rd from top
            \draw[->,thick,MyRed] (2.6,0.25) -- (4.775,0.25) -- (5.225,0.2947) -- (7.779,0.51); % 4th from top
            %% Virtual light rays top
            \draw[thick,MyRed,dashed] (1,-0.361) -- (5.7,1.9); % top
            \draw[thick,MyRed,dashed] (1,-0.274) -- (5.45,1.35); % 2nd from top
            \draw[thick,MyRed,dashed] (1,-0.1726) -- (5.3,0.817); % 3rd from top
            \draw[thick,MyRed,dashed] (1,-0.0636) -- (5.225,0.2947); % 4th from top
            %% Bottom
            \draw[->,thick,MyRed] (2.6,-1.75) -- (4.38,-1.75) -- (5.7,-1.9) -- (7.779,-2.9); % bottom
            \draw[->,thick,MyRed] (2.6,-1.25) -- (4.6,-1.25) -- (5.45,-1.35) -- (7.779,-2.1998); % 2nd from bottom
            \draw[->,thick,MyRed] (2.6,-0.75) -- (4.7,-0.75) -- (5.3,-0.817) -- (7.779,-1.3875); % 3rd from bottom
            \draw[->,thick,MyRed] (2.6,-0.25) -- (4.775,-0.25) -- (5.225,-0.2947) -- (7.779,-0.51); % 4th from bottom
            %% Virtual light rays bottom
            \draw[thick,MyRed,dashed] (1,0.361) -- (5.7,-1.9); % bottom
            \draw[thick,MyRed,dashed] (1,0.274) -- (5.45,-1.35); % 2nd from bottom
            \draw[thick,MyRed,dashed] (1,0.1726) -- (5.3,-0.817); % 3rd from bottom
            \draw[thick,MyRed,dashed] (1,0.0636) -- (5.225,-0.2947); % 4th from bottom

            % Point F
            \draw[draw=MyRed,fill=white,thick] (1.75,0) circle (0.075) node[above, shift={(0mm,2mm)}, text=MyBlack, fill=white] {$F$};
               
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Zerstreuungslinse.}
\end{figure}

Eine Zerstreuungslinse erzeugt stets aufrechte, verkleinerte, \textit{virtuelle} Bilder auf derselben Seite, wo sich der Gegenstand befindet. Nähert man den Gegenstand der Linsenoberfläche an, so bewegt sich auch 
    das Bild des Gegenstands auf die Linse zu und wird dabei grösser; es bleibt jedoch stets kleiner als das Original.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Filled areas
            \draw[fill=MyRed!50,draw=none,path fading=east] (1,1.5) -- (5,1.5) -- (6.5,2.4375) -- (6.5,-0.5625) -- cycle;
            
            % Arrows
            %% Gegenstand & Bild
            \draw[draw=none,fill=MyBlack!75] (3.45,0) -- (3.45,0.2125) -- (3.35,0.2125) -- (3.5,0.5625) -- (3.65,0.2125) -- (3.55,0.2125) -- (3.55,0) -- cycle node[anchor=west, shift={(1mm,2.25mm)}, text=MyBlack] {$B$}; % B (virtuell)
            \draw[draw=none,fill=MyBlack!75] (0.95,0) -- (0.95,1.15) -- (0.85,1.15) -- (1,1.5) -- (1.15,1.15) -- (1.05,1.15) -- (1.05,0) -- cycle; % G (reell)
            
            % Lens
            \draw[thick,draw=MyGreen,fill=MyGreen!60] (5.75,2) arc[start angle=150,delta angle=60,radius=4] -- (4.25,-2)  arc[start angle=-30,delta angle=60,radius=4] -- cycle;
            \draw[thick,white] (5,2.05) -- (5,-2.05);
            
            % Dash dotted line
            \draw[thick,dashdotted,MyBlack] (0,0) -- (10,0);

            % Lengths
            %% G
            \draw[thick,MyBlack] (1,1.5) -- (0,1.5);
            \draw[<->,thick,MyBlack] (0.5,0) -- (0.5,1.5) node[pos=0.5, anchor=east, shift={(0mm,0mm)}, text=MyBlack] {$G$};
            %% g
            \draw[thick,MyBlack] (1,-0.5) -- (1,-3.5);
            \draw[thick,MyBlack,dashed] (5,-1.5) -- (5,-3.5);
            \draw[<->,thick,MyBlack] (1,-3.25) -- (5,-3.25) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$g$};
            %% f
            \draw[thick,MyBlack] (2.6,-0.75) -- (2.6,-2.75);
            \draw[<->,thick,MyBlack] (2.6,-2.5) -- (5,-2.5) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$f$};
            %% b
            \draw[thick,MyBlack] (3.5,-0.5) -- (3.5,-2);
            \draw[<->,thick,MyBlack] (3.5,-1.75) -- (5,-1.75) node[pos=0.5, anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$b$};

            % Light rays
            \draw[->,thick,MyRed] (1,1.5) -- (10,-1.875);
            \draw[->,thick,MyRed] (1,1.5) -- (5,1.5) -- (8.2,3.5);
            \draw[->,thick,MyRed] (1,1.5) -- (5,0.5625) -- (10,0.5625);

            \draw[thick,MyRed,dashed] (2.6,0) -- (5,1.5);
            \draw[thick,MyRed,dashed] (3.5,0.5625) -- (5,0.5625);
            \draw[thick,MyRed,dashed] (5,0.5625) -- (7.4,0);

            % Points
            \draw[draw=MyBlack,fill=white] (1,1.5) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$P$};
            \draw[draw=MyBlack,fill=white] (1,0) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$Q$};
            \draw[draw=MyBlack,fill=white] (3.5,0.5625) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$U$};
            \draw[draw=MyBlack,fill=white] (3.5,0) circle (0.05) node[anchor=north, shift={(0mm,0mm)}, text=MyBlack] {$V$};
            \draw[draw=MyBlack,fill=white] (5,0) circle (0.05) node[anchor=south, shift={(0mm,0mm)}, text=MyBlack] {$X$};

            \draw[draw=MyRed,fill=white] (2.6,0) circle (0.05) node[anchor=north, shift={(0mm,-1mm)}, text=MyBlack] {$F_1$};
            \draw[draw=MyRed,fill=white] (7.4,0) circle (0.05) node[anchor=north, shift={(0mm,-1mm)}, text=MyBlack] {$F_2$};

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Bildkonstruktion an der Zerstreuungslinse: virtuelles Bild.}
    \label{fig:Bildkonstruktion an der Zerstreuungslinse virtuelles Bild}
\end{figure}

Zur Konstruktion des Bildes zeichnet man zu einem Gegenstandspunkt wiederum einen Parallel- und einen Brennpunktstrahl ein. Der Parallelstrahl wird durch die Zerstreuungslinse so gebrochen, dass die nach hinten 
    gerichtete Verlängerung des Strahls durch den Brennpunkt verläuft. Das (stets verkleinerte) Bild befindet sich am Schnittpunkt (Abbildung \ref{fig:Bildkonstruktion an der Zerstreuungslinse virtuelles Bild}, Punkt \textit{U}) des so verlängerten Parallelstrahls mit dem Mittelpunktstrahl zwischen dem Gegenstand und der Linse.