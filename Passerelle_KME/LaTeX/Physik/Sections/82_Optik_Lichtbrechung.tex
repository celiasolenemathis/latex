\clearpage % flushes out all floats
\subsection{Lichtbrechung und Totalreflexion}

Neben der Spiegelung ist die Brechung von Licht einer Erscheinung, die wir im Alltag beobachten können. Taucht man zum Beispiel einen Löffel in ein Glas Wasser, so erscheint er an der Grenzfläche zwischen Luft und 
    Wasser (der Mediengrenze) nach oben geknickt. Diese Veränderung des optischen Bildes eines in Wasser getauchten Gegenstandes kann mit dem Brechungsgesetz erklärt werden.

\subsubsection{Das Brechungsgesetz von Snellius}

Geht ein Lichtstrahl von einem optischen dünneren zu einem optisch dichten Medium über, so wird er zum Lot \textit{hin} gebrochen. Der einfallende, der reflektierte und der gebrochene Strahl liegen in einer Ebene. 
    Dementsprechend wird ein Lichtstrahl, der von einem optisch dichten zu einem optisch dünneren Medium übergeht, vom Lot \textit{weg} gebrochen. \\\hspace*{\fill}

Diese beiden Formen des Brechungsgesetzes sind Spezialfälle eines allgemeinen Brechungsgesetzes: Beim Übergang von einem optischen Medium 1 (z.B. Wasser) zu einem optischen Medium 2 (z.B. Glas) wird ein Lichtstrahl 
    zum einen Teil reflektiert, zum anderen Teil abgelenkt (siehe Abbildung \ref{fig:Allgemeines Brechungsgesetz}). Dabei gilt das allgemeine Brechungsgesetz:
    \begin{equation}
        \frac{p_1}{p_2} = \frac{\sin(\alpha)_1}{\sin(\alpha)_2} = \frac{n_2}{n_1} = \frac{c_1}{c_2} \label{eq:Allgemeines Brechungsgesetz}
    \end{equation}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Bottom semicircle
            \begin{scope}
                \clip (1,-4) rectangle (9,0);
                \draw[draw=none,fill=MyGreen!30!MyGrey] (5,0) circle (4);
                \draw[thick,MyGreen!60!MyBlack] (1,0) -- (9,0);
            \end{scope}

            % Angles
            \draw[MyBlack,fill=MyLightBlue!30] (5,0) -- (6,1) arc (45:135:1.42) -- cycle;
            \draw[draw=none,fill=MyGreen!30] (5,0) -- (5,-1) arc (270:315:0.7) -- cycle; % fill
            \draw[MyBlack,fill=none] (5,-1) arc (270:315:0.7); % arc line
            
            % Circle & Lot
            \draw[dashed,draw=MyBlack,fill=none] (5,0) circle (4);
            \draw[dashed,draw=MyBlack,fill=none] (5,4.5) -- (5,-4.5);

            % Light rays
            \draw[->,very thick,MyOrange] (1.5,3.5) -- (5,0) node[pos=0, below, shift={(-10mm,0mm)}, text=MyBlack, align=left] {einfallender\\ Lichtstrahl};
            \draw[->,thick,MyRed] (5,0) -- (8.5,3.5) node[below, shift={(10mm,0mm)}, text=MyBlack, align=left] {reflektierter\\ Lichtstrahl};
            \draw[->,MyRed] (5,0) -- (7.5,-4) node[below, shift={(10mm,0mm)}, text=MyBlack, align=left] {gebrochener\\ Lichtstrahl};

            % Distances
            %% p_1
            \draw[<->,MyBlack] (2.18,2.82) -- (5,2.82) node[pos=0.5, above, shift={(0mm,0mm)}, text=MyBlack] {$p_1$};
            \draw[draw=MyBlack,fill=white] (2.18,2.82) circle (0.05);
            \draw[draw=MyBlack,fill=white] (5,2.82) circle (0.05);
            %% p_2
            \draw[<->,MyBlack] (5,-3.39) -- (7.1,-3.39) node[pos=0.5, above, shift={(0mm,0mm)}, text=MyBlack] {$p_2$};
            \draw[draw=MyBlack,fill=white] (5,-3.39) circle (0.05);
            \draw[draw=MyBlack,fill=white] (7.1,-3.39) circle (0.05);
            
            % Text
            \node[color=MyBlack] at (4.65,0.75) {$a_1$};
            \node[color=MyBlack] at (5.35,0.75) {$a_1$};
            \node[color=MyBlack] at (5.35,-1.2) {$a_2$};

            \node[color=MyBlack] at (7.5,1.25) {$n_1$};
            \node[color=MyBlack] at (7.5,-1.25) {$n_2$};

            \node[color=MyBlack] at (2.25,0.25) {Medium 1};
            \node[color=MyBlack] at (2.25,-0.25) {Medium 2};

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Allgemeines Brechungsgesetz.}
    \label{fig:Allgemeines Brechungsgesetz}
\end{figure}
\newpage

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Bleistift ist in Wasser getaucht (siehe Abbildung).
\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Container
            \draw[thick,MyBlack] (0.25,2) -- (0.5,2) -- (0.5,-2) -- (4.5,-2) -- (4.5,2) -- (4.75,2);
            \draw[thick,MyDarkBlue] (0.5,1.5) -- (4.5,1.5);
    
            % Pen
            \draw[fill=MyLightGrey,draw=none,rotate around={-28:(2,-2)}] (2,-2) -- ++(-0.25,0.75) -- ++(0,4.25) -- ++(0.5,0) -- ++(0,-4.25) -- cycle;
            
            % Angles
            \draw[draw=none,fill=MyRed!30] (2.95,1.5) -- (3.23,2) arc (62:90:0.6) -- cycle;
            \draw[MyBlack] (3.23,2) arc (62:90:0.6);

            \draw[draw=none,fill=MyOrange!30] (2.95,1.5) -- (2.95,1) arc (270:250:0.4) -- cycle;
            \draw[MyBlack] (2.95,1) arc (270:250:0.4);

            \draw[draw=none,fill=MyYellow!30] (2.95,1.5) -- (2.5,0.65) arc (230:250:0.55) --cycle;
            \draw[MyBlack] (2.5,0.65) arc (230:250:0.55);
            
            % Lot
            \draw[MyBlack,dashed] (2.95,2.5) -- (2.95,0.25);

            % Light ray
            \draw[dashed,MyRed] (3.5,2.5) -- (1.85,-0.5);
            \draw[->,thick,MyRed] (3.5,2.5) -- (2.95,1.5) -- (2.13,-1.25);
            
            % Text
            \node[color=MyBlack] at (3.15,2.25) {$\alpha$};
            \node[color=MyBlack] at (2.825,0.75) {$\beta$};
            \node[color=MyBlack] at (2.5,0.45) {$\epsilon$};
    
            \node[color=MyBlack] at (1.5,2) {$n_{Luft}$};
            \node[color=MyBlack] at (3.75,-1.5) {$n_{H_2O}$};
            
        \end{tikzpicture}
    \end{center}
\end{figure}

\textit{Gesucht:} Unter welchem Winkel erscheint er nach oben geknickt (d.h. der Winkel $\epsilon$ ist gesucht), wenn der Einfallswinkel $\alpha = 30\si{\degree}$ ist, der Brechungsindex von Wasser 1.33 und der von 
    Luft 1 ist? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    \frac{\sin(\alpha)}{\sin(\beta)} &= \frac{n_{H_2O}}{n_{Luft}} \ \Rightarrow \beta = \arcsin\left(\frac{\sin(\alpha) \cdot n_{Luft}}{n_{H_2O}}\right) \\                   
                            \epsilon &= \alpha - \beta = 30\si{\degree} - \arcsin\left(\frac{\sin(30\si{\degree}) \cdot 1}{1.33}\right) = \underline{\underline{7.92\si{\degree}}}
\end{align*}

\subparagraph{Beispiel 2}
\textit{Gegeben:} Im Vakuum beträgt die Lichtgeschwindigkeit $c = 3 \cdot 10^8\si{\metre/\second}$. \\\hspace*{\fill}

\textit{Gesucht:} Wie gross ist die Lichtgeschwindigkeit im Wasser? \\\hspace*{\fill}

\textit{Lösung:}
\[
    c_{H_2O} = \frac{c_{Vakuum}}{n_{H_2O}} = \frac{3 \cdot 10^8\si{\metre/\second}}{1.33} = \underline{\underline{2.26 \cdot 10^8\si{\metre/\second}}}
\]

\subparagraph{Beispiel 3}
\textit{Gegeben:} Licht konnte in einem speziell präparierten Gas auf $17\si{\metre/\second}$ abgebremst werden. \\\hspace*{\fill}

\textit{Gesucht:} Berechnen Sie den Brechungsindex des Gases. \\\hspace*{\fill}

\textit{Lösung:}
\[
    c_{Gas} = \frac{c_{Vakuum}}{n_{Gas}} \ \Rightarrow \ n_{Gas} = \frac{c_{Vakuum}}{c_{Gas}} = \frac{3 \cdot 10^8\si{\metre/\second}}{17\si{\metre/\second}} = \underline{\underline{1.76 \cdot 10^7}}
\]
\newpage

\subsubsection{Die Totalreflexion und der Grenzwinkel}

Tritt ein Lichtstrahl von einem optischen Medium mit hoher Brechzahl $n_1$ in einen anderen Stoff mit niedriger Brechzahl $n_2$ über, so wird er vom Lot weg gebrochen; der Brechungswinkel $\alpha_2$ ist in diesem 
    Fall grösser als der Einfallswinkel $\alpha_1$. Bei einem bestimmten, von den beiden Materialien abhängigen Einfallswinkel $\phi$ nimmt der Brechungswinkel $\alpha_2$ den Wert $90\si{\degree}$ an. In diesem Fall kann der einfallende Lichtstrahl $\alpha_1$ nicht mehr aus dem optisch dichteren Medium in das optisch dünnere Medium übergehen, sondern wird reflektiert beziehungsweise verläuft längs der Grenzfläche beider Medien. Ist der Einfallswinkel grösser oder gleich dem Grenzwinkel ($\alpha_1 \geq \phi$), so gibt es nur noch den reflektierten Strahl: Wir sprechen von Totalreflexion.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Left Image
            %% Mirror
            \draw[draw=none,fill=MyBlack!75!white] (-1,0) rectangle (2,0.125);
            
            %% Angles
            \draw[draw=none,fill=MyRed!30] (0.5,0.0625) -- (0.5,-1.5) arc (270:230:1) -- cycle;
            \draw[MyBlack] (0.5,-1.5) arc (270:230:1);
            
            \draw[draw=none,fill=MyRed!30] (0.5,0.0625) -- (0.5,1.5) arc (90:50:1.7) -- cycle;
            \draw[MyBlack] (0.5,1.5) arc (90:50:1.7);
            
            \draw[MyBlack,dashed] (0.5,2.125) -- (0.5,-2); % Lot

            %% Light ray
            \draw[->,thick,MyRed] (-0.5,-2) -- (0.5,0.0625);
            \draw[->,thick,MyRed] (0.5,0.0625) -- (2,1.5);
            \draw[->,MyRed] (0.5,0.0625) -- (1.5,-2);
            
            %% Text
            \node[color=MyBlack] at (-0.5,1.125) {$n_2$};
            \node[color=MyBlack] at (-0.5,-1) {$n_1$};

            \node[color=MyBlack] at (0.25,-1.15) {$\alpha_1$};
            \node[color=MyBlack] at (1,1.15) {$\alpha_2$};

            \node[color=MyBlack] at (0.5,-2.25) {$\alpha_1 < \phi$};

            % Middle Image
            %% Mirror
            \draw[draw=none,fill=MyBlack!75!white] (3.5,0) rectangle (6.5,0.125);
            
            %% Angles
            \draw[draw=none,fill=MyRed!30] (5,0.0625) -- (5,-1.5) arc (270:230:1.35) -- cycle;
            \draw[MyBlack] (5,-1.5) arc (270:230:1.35);
            
            \draw[draw=none,fill=MyRed!30] (5,0.0625) -- (5,1.375) arc (90:0:1.25) -- (6.25,0.0625) -- cycle;
            \draw[MyBlack] (5,1.375) arc (90:0:1.25) -- (6.25,0.0625);
            
            \draw[MyBlack] (5.5,0.0625) arc (360:270:0.5);
            \draw[fill=MyBlack,draw=none]  (5.2,-0.2) circle (0.025);

            \draw[MyBlack,dashed] (5,2.125) -- (5,-2); % Lot

            %% Light ray
            \draw[->,thick,MyRed] (3.75,-1.75) -- (5,0.0625);
            \draw[->,thick,MyRed] (5,0.0625) -- (6.5,0.0625);
            
            %% Text
            \node[color=MyBlack] at (4,0.875) {$n_2$};
            \node[color=MyBlack] at (4,-0.75) {$n_1$};

            \node[color=MyBlack] at (4.65,-1.15) {$\phi$};
            \node[color=MyBlack] at (5.6,0.35) {\scriptsize $\alpha_2 = 90\si{\degree}$};

            \node[color=MyBlack] at (5,-2.25) {$\alpha_1 = \phi$ Grenzwinkel};

            % Right Image
            %% Mirror
            \draw[draw=none,fill=MyBlack!75!white] (8,0) rectangle (11,0.125);
            
            %% Angles
            \draw[draw=none,fill=MyRed!30] (9.5,0.0625) -- (8.5,-1) arc (225:315:1.425) -- cycle;
            \draw[MyBlack] (8.5,-1) arc (225:315:1.425);

            \draw[MyBlack,dashed] (9.5,2.125) -- (9.5,-2); % Lot
            
            \draw[draw=MyBlack,fill=white] (9.5,-1.42) circle (0.05);

            %% Light ray
            \draw[->,thick,MyRed] (8,-1.5) -- (9.5,0.0625);
            \draw[->,thick,MyRed] (9.5,0.0625) -- (11,-1.5);
            
            %% Text
            \node[color=MyBlack] at (8.5,0.625) {$n_2$};
            \node[color=MyBlack] at (8.5,-0.5) {$n_1$};

            \node[color=MyBlack] at (9.15,-1) {$\alpha_1$};
            \node[color=MyBlack] at (9.85,-1) {$\alpha_1$};

            \node[color=MyBlack] at (9.5,-2.25) {$\alpha_1 > \phi$ Totalreflexion};

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Übergang vom optisch dichteren zum optisch dünneren Medium: Totalreflexion.}
\end{figure}

Bei der Totalreflexion, welche nur möglich ist, wenn der Lichtstrahl von einem dichteren zu einem dünneren Medium übergeht, erreicht man einen sogenannten \textit{Grenzwinkel}, d.h., wenn dieser erreicht ist, bleibt 
    das Licht im Medium, in dem es gestartet ist. Der Grenzwinkel für die Totalreflexion $\alpha_1 = \phi$ kann mithilfe der folgenden Formel leicht bestimmt werden:
    \begin{equation}
        \frac{\sin(\phi)}{\sin(90\si{\degree})} = \frac{n_2}{n_1} \ \Rightarrow \ \phi = \arcsin\left(\frac{n_2}{n_1}\right) \label{eq:Totalreflexion}
    \end{equation}

Wichtig: $n_1$ bezeichnet den Brechungsindex des Mediums, in dem der Lichtstrahl gestartet ist und $n_2$ den Brechungsindex des Mediums, wo der Lichtstrahl hingeht.