
\subsection{Definition}
    Sei $f(x)$ darstellbar als das Produkt zweier differenzierbaren Funktionen $u(x)$ und $v(x)$, so dass gilt:
    $$ f(x)=u(x)\cdot v(x) $$
    lässt sich die Ableitung $f'(x)$ wie folgt beschreiben:
    $$ f'(x) = (u(x) \cdot v(x))' = u'(x) \cdot v(x)+u(x) \cdot v'(x)$$ 
    oder in Kurzform:
    $$ f'(x) = (uv)' = u'v+uv'$$ 
\subsection{Beispiel}
    Gesucht sei die Ableitung $f'(x)$ der gegebenen Funktion $f(x)= x^2 \cdot 4x$.
    Die Funktion $f(x)$ lässt sich aufspalten in $u(x)=x^2$ und $v(x)=4x$ mit den 
    dazugehörigen Ableitungen nach Exponentenregel: $u'(x)=2x$ resp. $v'(x)=4$.
    Nach der Produktregel folgt die Ableitung:
    $$ \underline{\underline{f'(x)}}=(u(x) \cdot v(x))'= (x^2 \cdot 4x)' = u'(x) \cdot v(x)+u(x) \cdot v'(x) = 2x \cdot 4x + x^2 \cdot 4 = \underline{\underline{12x^2}} $$

\newpage
\subsection {Herleitung}
    Die Herleitung der Produktregel lässt sich sehr schön grafisch veranschaulichen. Das Produkt der beiden Funktionen $u(x)$ und $v(x)$
    lässt sich auch als Fläche eines Rechtecks darstellen, sodass gilt $A(x)=f(x)=u(x)\cdot v(x)$. Die X-Achse stellt nun also die Funktionswerte von $u(x)$ dar, 
    die Y-Achse diejenigen von $v(x)$.
    \begin{center}
        \begin{tikzpicture}
            \begin{axis}[   samples=100,
                            inner axis line style={thick},
                            axis lines=center,
                            axis line style={-Latex},
                            xticklabels={,,},
                            yticklabels={,,},
                            xlabel={$u$},
                            ylabel={$v$},
                            ymax=3.5,
                            ymin=-1,
                            xmin=-1,
                            xmax=3.5,
                            width=8cm,
                            height=8cm
                            ]
            
            \fill[blue!50] (axis cs:0,0) rectangle (axis cs:2.5,2);
            \fill[blue!60] (axis cs:0,2) rectangle (axis cs:2.5,2.5);
            \fill[blue!60] (axis cs:2.5,0) rectangle (axis cs:3,2);
            \fill[blue!80] (axis cs:2.5,2) rectangle (axis cs:3,2.5);

            \node at (axis cs:1.25, 1){$u(x)\cdot v(x)$};
            \node at (axis cs:1.25, -0.25){\Large{$\underbrace{\phantom{XXXXXXXI}}_{u(x)}$}};
            \node at (axis cs:2.75, -0.35){\Large{$\underbrace{}_{\Delta u(x)}$}};
            \node[rotate=-90] at (axis cs:-0.4, 1){$\underbrace{\phantom{XXXXXXXX}}_{\rotatebox{90}{$v(x)$}}$};
            \node[rotate=-90] at (axis cs:-0.6, 2.25){$\underbrace{}_{\rotatebox{90}{$\Delta v(x)$}}$};
            \node at (axis cs:1.25, 2.25){$u(x)\cdot \Delta v(x)$};
            \node[rotate=90] at (axis cs:2.75, 1){$v(x)\cdot \Delta u(x)$};

            \end{axis}
        \end{tikzpicture}
    \end{center}

    Die Frage nach der Ableitung von $f(x)$ lässt sich nun also auch so formulieren:\\
    Wie stark ändert sich die Fläche des grossen Rechtecks, wenn sich $u(x)$ resp. $v(x)$ 
    um einen gewissen Betrag ändert?\\

    Man stelle sich vor, $u(x)$ sowie $v(x)$ werden jeweils um einen kleinen Betrag $\Delta$ erweitert.
    An der Grafik erkennt man, welchen Einfluss das auf die Rechtecksfläche hat. Die Flächenänderung beträgt

    $$ \Delta f(x) = u(x)\cdot \Delta v(x)+v(x)\cdot \Delta u(x) +  \Delta v(x)\Delta u(x).$$

    Wie gross $\Delta u(x)$ und $\Delta v(x)$ jeweils sind, ist natürlich von $\Delta x$ abhängig. 
    Das Verhältnis $\frac{\Delta u(x)}{\Delta x}$ an der Stelle $x_0$ entspricht der Sekantensteigung und schlussendlich der Tangentensteigung 
    an diesem Punkt wenn $\lim\limits_{\Delta x \rightarrow 0}$. Sie ist somit nichts anderes 
    als $u'(x_0)$.\\
    
    Folglich lässt sich $\Delta u(x)$ auch als $u'(x) \cdot \Delta x$ schreiben.
    Gleiches gilt natürlich auch für $\Delta v(x)$ und $\Delta f(x)$. In obige Gleichung eingesetzt ergibt das:
    $$ f'(x) \cdot \Delta x = u(x)\cdot v'(x) \cdot \Delta x +v(x)\cdot u'(x) \cdot \Delta x +  v'(x) \cdot \Delta x \cdot u'(x) \cdot \Delta x$$
    Es lässt sich weiter vereinfachen:
    \begin{align*}
        f'(x) \cdot \cancel{\Delta x} &= u(x)\cdot v'(x) \cdot \cancel{\Delta x} + v(x)\cdot u'(x) \cdot \cancel{\Delta x} +  v'(x) \cdot \cancel{\Delta x} \cdot u'(x) \cdot \Delta x\\
        f'(x) &= u(x)\cdot v'(x)+v(x)\cdot u'(x) +  v'(x) \cdot u'(x) \cdot \Delta x
    \end{align*}
    Obige Überlegungen der Tangentensteigungen sind nur gültig, wenn $\Delta x$ eine infinitesimale (also unendlich kleine) Grösse ist.
    Somit fällt der letzte Term weg:

    $$f'(x) = \lim\limits_{\Delta x \rightarrow 0} u(x)\cdot v'(x)+v(x)\cdot u'(x) +  \cancel{v'(x) \cdot u'(x) \cdot \Delta x}$$
    $$\boxed{f'(x) = u(x)\cdot v'(x)+v(x)\cdot u'(x)} $$

    $\blacksquare$.
    

