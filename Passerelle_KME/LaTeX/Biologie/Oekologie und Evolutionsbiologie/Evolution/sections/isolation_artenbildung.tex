\section{Isolation und Artbildung}
\subsection{Entstehung neuer Arten}
Da es nicht so einfach ist den Begriff der Art als Konzept zu definieren, hält sich das Compendio-Buch an die Definition der Art als Fortpflanzungsgemeinschaft:
\begin{center}
    \textbf{Individuen, die sich untereinander fortpflanzen können und fortpflanzungsfähige Nachkommen bilden, gehören der gleichen Art an.}
\end{center}
Diese Definition ist zwar intuitiv verständlich, schliesst aber vegetativ fortpflanzende Lebewesen und Fossilien aus.\\
Jedoch sind im Kontext der Evolution die sexuell fortpflanzenden Lebewesen von besonderem Interesse. Bei der Artbildung unterscheidet man folgende Fälle:
\begin{itemize}
    \item \textbf{Allopatrische} Artbildung
    \item \textbf{Sympatrische} Artbildung
\end{itemize}
Beide Artbildungen haben gemeinsam, dass der Genfluss zwischen zwei Populationen verhindert wird.

\newpage

\subsubsection*{Allopatrische Artbildung}
Die Ursache für allopatrische Artbildung ist die \textbf{räumliche Trennung}. Essentiell dabei ist, dass eine unüberwindbare Barriere entsteht, die von einer Art nicht mehr überwunden werden kann. Räumliche Distanz ist dabei zweitrangig.\\
Eine Art wird in \textbf{zwei} (oder mehrere) \textbf{Populationen} unterteilt. Ein genetischer Austausch ist nicht mehr möglich und durch Gendrift und Selektion verändert sich der jeweilige Genpool einer Population in eine andere Richtung.
Verschwindet die Barriere nach einiger Zeit wieder, kann es sein, dass sich die neu entstanden Arten \textbf{nicht mehr miteinander fortpflanzen} oder \textbf{Hybride} zeugen können. Jedoch besitzen Hybride meist eine tiefere Fitness und werden deswegen von der Selektion benachteiligt.\\
Können sich die beiden Populationen noch hybridisieren, spricht man von zwei verschiedenen Unterarten und nicht von zwei separaten Arten. Beispiele sind:
\begin{itemize}
    \item Raben- und Nebelkrähen $\rightarrow$ Können sich noch hybridisieren.
    \item Süd- und Nordeichhörnchen $\rightarrow$ Können sich nicht mehr hybridisieren.
\end{itemize}

\subsubsection*{Sympatrische Artbildung}
Bei der sympatrischen Artbildung entstehen neue Arten im \textbf{selben Lebensraum}. Die Trennung erfolgt durch \textbf{ökologische Barrieren}, begleitet von weiteren Mechanismen, die die Paarungsfreiheit einschränken.\\
Ein Beispiel für sympatrische Artbildung sind die Zitronen- und Pfeilbuntbarsch im Vulkankratersee \textit{Apoyo} in Nicaragua. Der ökologische Grund für die Artbildung ist, dass Zitronenbuntbarsche den felsigen Grund des Sees bewohnen, während Pfeilbuntbarsche das offene Wasser bevorzugen.\\
Bei Pflanzen kann es durch spontane Polyploidisierung zu sympatrischer Artbildung kommen, wenn durch Selbstbefruchtung oder vegetativer Fortpflanzung genügend fruchtbare Nachkommen aus einem Urexemplar entstehen.

\subsection{Isolationsmechanismen}
Isolationsmechanismen sind Faktoren, die eine \textbf{genetische Vermischung} zweier Populationen/Arten \textbf{verhindern} und somit ihr Vorkommen im selben geografischen Raum ermöglichen.\\
Von \textbf{präzygotischen Isolationsmechanismen} spricht man, wenn \textbf{keine Begattung} oder \textbf{Befruchtung} möglich ist. Bei \textbf{postzygotischen Isolationsmechanismen} kann eine \textbf{Zygote} gebildet werden, der Hybrid ist aber entweder \textbf{nicht lebensfähig}, \textbf{geschwächt} oder \textbf{steril}. Ein solcher Hybrid hat eine geringere Fitness als die jeweiligen (Unter-)Arten, wodurch eine Zusammenführung der Arten im natürlichen Umfeld utopisch wird.

\subsubsection*{Präzygotische Isolation}
Beispiele der präzygotischen Isolation:
\begin{itemize}
    \item ökologische Isolation
    \item tages- oder jahreszeitliche Isolation
    \item mechanische Isolation
    \item ethologische Isolation
\end{itemize}

\paragraph*{Ökologische Isolation} beschreibt den Zustand, wenn zwei Arten das selbe Gebiet bewohnen, aber \textbf{verschiedene ökologische Nischen} beanspruchen. Aufgrund der verschiedenen Ansprüche kommt es selten zu einer Kreuzung und die entstehenden Hybride sind benachteiligt, da sie an keine der beiden Nischen voll angepasst sind.

\paragraph*{Tages- oder jahreszeitliche Isolation} kommt dann zum Zug, wenn zwei Arten unterschiedliche \textbf{Tages- oder Jahreszeit abhängige Paarungsbereitschaft} haben.

\paragraph*{Mechanische Isolation} entsteht dann, wenn die Befruchtung durch \textbf{Unterschiede} in der \textbf{Körpergrösse} oder im \textbf{Bau} der \textbf{Geschlechtsorgane} unmöglich oder nur eingeschränkt möglich ist.

\paragraph*{Ethologische Isolation} ist die Folge davon, wenn zwei Arten \textbf{unterschiedliche Verhaltensweisen} für das \textbf{Erkennen} von \textbf{Sexualpartnern} entwickeln.

\subsection{Adaptive Radiation}
Adaptive Radiation bezeichnet den Vorgang, wenn sich \textbf{eine Ausgangsart schnell in verschiedene Arten aufspaltet}.\\
Typische Merkmale adaptiver Radiation sind:
\begin{itemize}
    \item \textbf{Artaufspaltung}: Aus einer Art entstehen zahlreiche neue Arten.
    \item \textbf{Schneller Ablauf}, für geologische Zeitverhältnisse.
    \item Es werden \textbf{verschiedene ökologischen Nischen} besetzt (hauptsächlich Nahrungsnischen).
    \item Die neuen Arten \textbf{unterscheiden} sich \textbf{phänotypisch}, infolge der Anpassung an die ökologischen Nischen.
\end{itemize}
Das berühmteste Beispiel für adaptive Radiation sind die Darwinfinken, deren Vorfahren vor ca. $3$ Mio. Jahren die Galapagos-Inseln besiedelten und durch adaptive Radiation schnell die vorhandenen Nischen besetzten und sich in $14$ verschieden Arten entwickelten.\\
Gründe für adaptive Radiation sind:
\begin{itemize}
    \item \textbf{Geografische Isolation} in einem Lebensraum mit offenen Nischen.
    \item Besiedlung eines \textbf{neu entstandenen Lebensraums}.
    \item \textbf{Neubesetzung} der Nischen nach einem \textbf{Massenaussterben}.
    \item \textbf{Neubesiedlung} eines Lebensraums dank \textbf{evolutionären Neuerfindungen}.
\end{itemize}
Evolutionäre Neuerfindung eröffnen neue Lebensräume für verschiedene Arten. Beispiele für solche Neuerfindungen wäre die Entwicklung der Flügel bei Dinosauriern oder die Adaption an ein Leben im Wasser, wodurch aus Urpaarhufern Wale entstanden.

\subsection{Synthetische Evolutionstheorie}
Die Synthetische Evolutionstheorie vereinigt Darwins Lehre mit der Genetik und verschiedenen anderen biologischen Disziplinen. Sie wird deswegen auch \textit{Neodarwinismus} genannt.\\
Ihr grosser Vorteil gegenüber der ursprünglichen Evolutionstheorie von Darwin ist, dass sie die Gründe für die auftretende genetische Variabilität erklären kann.