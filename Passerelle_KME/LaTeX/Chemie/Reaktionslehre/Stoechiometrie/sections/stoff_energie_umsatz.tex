\section{Stoff- und Energieumsatz chemischer Reaktionen}
\subsection{Reaktionsgleichung}
Die Reaktionsgleichung beschreibt einen chemischen Vorgang qualitativ und quantitativ. Links vom Reaktionspfeil stehen die Edukte, rechts die Produkte.\\
\begin{center}
    \ch{!(Edukte)( 2 Al + 3 Cl2 ) -> !(Produkte)( 2 AlCl3 )} \par
\end{center}

Der Reaktionspfeil steht für die chemische Umwandlung der Stoffe. Über dem Pfeil können Angaben zu Reaktionsbedingungen stehen.\\
Die Koeffizienten vor den Formeln geben das Verhältnis der Stoffmengen bzw. der Teilchenzahlen an; der Koeffizient 1 wird nicht geschrieben. Die Koeffizienten müssen beim Aufstellen einer Reaktionsgleichung so gewählt werden, dass die Zahl der Atome jeder Sorte\footnote{Koeffizient $\cdot$ Index} auf beiden Seiten gleich gross ist. Die Indices gehören zur Formel eines Stoffs und beschreiben seine Zusammensetzung.

\subsection{Mengen und Massenverhältnis bei Reaktionen}
Aus dem Stoffmengenverhältnis, das in der Reaktionsgleichung durch die Koeffizienten ausgedrückt wird, lässt sich durch Multiplikation mit der molaren Masse das Massenverhältnis der beteiligten Stoffportionen berechnen.
Ist die Masse oder die Menge eines Reaktionsteilnehmers gegeben, lassen sich die Massen und die Mengen der anderen berechnen.

\subsubsection*{Beispiel}
Menge an Schwefel, die mit 10g Eisen reagieren.
\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {p{2cm} p{1.2cm} p{0.5cm} p{1.2cm} p{0.5cm} p{1.2cm}}
        Gleichung:   & \ch{1 Fe}      & $+$ & \ch{1 S}       & $\longrightarrow$ & \ch{1 FeS}     \\
        Stoffmengen: & $1\si{\mole}$  &     & $1\si{\mole}$  &                   & $1\si{\mole}$  \\
        Massen:      & $55.85\si{\g}$ &     & $32.06\si{\g}$ &                   & $87.91\si{\g}$
    \end{tabular}
\end{flushleft}

\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {p{4cm} p{1.5cm} p{4cm}}
        Mit $55.85\si{\g}$ Eisen reagieren &                                                     & $32.06\si{\g}$ Schwefel \\
        Mit $10\si{\g}$ Eisen reagieren    & $\frac{32.06\si{\g} \cdot 10\si{\g}}{55.85\si{\g}}$ & $~~5.74\si{\g}$ Schwefel
    \end{tabular}
\end{flushleft}

\subsubsection*{Dynamisches Gleichgewicht}
Die Gleichgewichtskonzentration eines Stoffes lässt sich Anhand seiner Anfangskonzentration und der Gleichgewichtskonzentration seines Reaktionspartners/Produkts errechnen.
\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {l l}
        Reaktionsgleichung & \ch{2 NO2 <=> N2O4} \\
        Anfangskonzentration & \ch{NO2} $= ~ 0.26\si{\mole\per\liter}$\\
        Gleichgewicht bei & \ch{c(N2O4)} $= ~ 0.08\si{\mole\per\liter}$\\
    \end{tabular}
\end{flushleft}

\begin{flushleft}
    \begin{tikzpicture}
        \matrix[column sep=0.5cm, row sep=0.25cm] (m) {
            \node {};
            & \node[text centered] {\ch{NO2}};
            & \node[text centered] (headerN2O4) {\ch{N2O4}}; \\

            \node[text width = 1cm] {c(Anf.)};
            & \node[]{$0.26\si{\mole\per\liter}$};
            & \node[]{/}; \\

            \node[text width = 1cm] {c(GGW.)};
            & \node[] (ggwNO2) {$0.10\si{\mole\per\liter}$};
            & \node[] (ggwN2O4) {$0.08\si{\mole\per\liter}$};\\
        };
        
        \node[right = 1.5cm of headerN2O4] (calculation) {$0.26 - 2 \cdot 0.08 = 0.10$};
        \node[below right = 0cm and -3.5cm of calculation, text width = 5.5cm] {{\scriptsize Verhältnis aus chemischen Gleichgewicht bleibt erhalten.}};

        \node[below right = 1.5cm and -3.5cm of calculation] {$K = \frac{0.08\si{\mole/\liter}}{(0.1\si{\mole/\liter})^2} = 8\si{\liter\per\mole}$};
    \end{tikzpicture}
\end{flushleft}

\subsection{Volumenverhältnisse bei Gasreaktionen}
\subsubsection*{Volumen und Teilchenzahl bei Gasen}
Für die gasförmigen Reaktionsteilnehmer ist das Verhältnis der Volumina aus den Koeffizienten der Reaktionsteilnehmer ersichtlich. Es entspricht dem Verhältnis der Teilchenzahlen und der Stoffmenge.
\begin{center}
    \textcolor{darkblue}{1}\ch{!(\textcolor{darkblue}{$1$}$\si{\liter}$)( N2 )}
    \ch{+}
    \textcolor{darkgreen}{3}\ch{!(\textcolor{darkgreen}{$3$}$\si{\liter}$)( H2 )}
    \ch{->}
    \textcolor{darkred}{2}\ch{!(\textcolor{darkred}{$2$}$\si{\liter}$)( NH3 )} \par
\end{center}

\subsubsection*{Molare Masse}
$1\si{\mole}$ eines beliebigen Gases enthält $6.02 \cdot 10^{23}$ Teilchen und hat bei Normalbedingungen ein Volumen vom $22.4\si{\liter}$. Das molare Volumen ist (bei Normalbedingungen\footnote{Als Normalbedingungen sind eine Temperatur von \SI{25}{\degreeCelsius} (\SI{298}{\kelvin}) und ein Luftdruck von $1$atm (\SI{1.013e5}{\pascal}) definiert.}) für alle Gase $\SI{22.4}{\liter\per\mole}$.\\
Die Masse einer Gasportion von $1\si{\mole}$ in g entspricht zahlenmässig der Teilchenmasse in $u$.

\subsection{Energieumsatz bei chemischen Reaktionen}
Bei chemischen Vorgängen wird Energie umgesetzt, weil sich Produkte und Edukte im Energiegehalt unterscheiden.\\
Die Wärme, die bei konstantem Druck mit der Umgebung ausgetauscht wird, heisst Reaktionsenthalpie ($\Delta$H). Die Reaktionsenthalpie (in $\si{\joule\per\mole}$) bezieht sich auf die in der entsprechenden Reaktionsgleichung gegebenen Stoffe, Stoffmengen und Aggregatszustände.
    \begin{equation*}
        \Delta\text{H} = \text{H}(\text{Produkte}) - \text{H}(\text{Edukte})
    \end{equation*}

\begin{figure}[h!]
    \centering
    \hbox{
        \hspace{-1cm}
    \begin{tikzpicture}
        \begin{axis}[   
                samples=100,
                inner axis line style={thick},
                axis lines=center,
                axis line style={-Latex},
                xticklabels={,,},
                yticklabels={,,},
                xlabel={Reaktionsverlauf},
                ylabel={Enthalpie H},
                x label style={at={(axis description cs:0.5,-0.1)},anchor=north},
                y label style={at={(axis description cs:0,.5)},rotate=90,anchor=south},
                xmin=0,
                xmax=3,
                ymin=0,
                ymax=3.5,
                clip=false
            ]
            \node[above,font=\large\bfseries] at (current bounding box.north) {exotherme Reaktion};

            \coordinate(graphStart) at (axis cs: 0,1.3);
            \coordinate(reactionStart) at (axis cs: 1,1.3);
            \coordinate(peakWithoutCat) at (axis cs: 1.45,3);
            \coordinate(peakWithCat) at (axis cs: 1.45,2.25);
            \coordinate(reactionEnd) at (axis cs: 2.05, 0.4);
            \coordinate(graphEnd) at (axis cs: 3, 0.4);

            \draw[thick, dotted, gray] (axis cs: 0,0.4) -- (reactionEnd);
            \draw[thick, dotted, gray] (reactionStart) -- (axis cs: 3,1.3);
            \draw[thick, dotted, gray] (axis cs: 0,2.25) -- (axis cs: 3,2.25);
            \draw[thick, dotted, gray] (axis cs: 0,3) -- (axis cs: 3,3);
            \draw[thick, dotted, gray] (axis cs: 1,0) -- (reactionStart);
            \draw[thick, dotted, gray] (axis cs: 2.05, 0) -- (reactionEnd);

            \draw[ultra thick, dotted, rounded corners] (graphStart) -- (reactionStart) to[out=90,in=210] (peakWithCat) to[out=-30,in=100] (reactionEnd) -- (graphEnd);
            \draw[ultra thick, darkblue, rounded corners] (graphStart) -- (reactionStart) to[out=90,in=210] (peakWithoutCat) to[out=-30,in=100] (reactionEnd) -- (graphEnd);

            \path (graphStart) -- (reactionStart) node [above, midway] {{\small H (Edukte)}};
            \path (reactionEnd) -- (graphEnd) node [above, midway] {{\small H (Produkte)}};
            \node[text centered, above] at (axis cs: 1.45,3) {{\small H (Übergangszustand)}};

            \draw[->, > = stealth, thick, dashed] ($(graphStart)!0.5!(reactionStart)$) -- ($(axis cs: 0,0.4)!0.5!(axis cs: 1,0.4)$) node[right, midway] {{\small $\Delta$H}};
            \draw[->, > = stealth, thick, dashed] (axis cs: 2.1, 1.3) -- (axis cs: 2.1, 3) node[below, midway, sloped] {{\footnotesize Aktivierungsenergie}};
            \draw[->, > = stealth, thick, dashed] (axis cs: 2.5, 1.3) -- (axis cs: 2.5, 2.25) node[below, midway, sloped] {{\footnotesize AE mit Kat.}};

            \node[below, text centered] at (axis cs: 1,0) {{\small Start}};
            \node[below, text centered] at (axis cs: 2.05,0) {{\small Ende}};
        \end{axis}
    \end{tikzpicture}

    \hspace{0.5cm}

    \begin{tikzpicture}
        \begin{axis}[   
                samples=100,
                inner axis line style={thick},
                axis lines=center,
                axis line style={-Latex},
                xticklabels={,,},
                yticklabels={,,},
                xlabel={Reaktionsverlauf},
                ylabel={Enthalpie H},
                x label style={at={(axis description cs:0.5,-0.1)},anchor=north},
                y label style={at={(axis description cs:0,.5)},rotate=90,anchor=south},
                xmin=0,
                xmax=3,
                ymin=0,
                ymax=3.5,
                clip=false
            ]
            \node[above,font=\large\bfseries] at (current bounding box.north) {endotherme Reaktion};

            \coordinate(graphStart) at (axis cs: 0,0.4);
            \coordinate(reactionStart) at (axis cs: 1,0.4);
            \coordinate(peak) at (axis cs: 1.6,3);
            \coordinate(reactionEnd) at (axis cs: 2.05, 1.8);
            \coordinate(graphEnd) at (axis cs: 3, 1.8);

            \draw[thick, dotted, gray] (axis cs: 0,1.8) -- (reactionEnd);
            \draw[thick, dotted, gray] (reactionStart) -- (axis cs: 3,0.4);
            \draw[thick, dotted, gray] (axis cs: 0,3) -- (axis cs: 3,3);
            \draw[thick, dotted, gray] (axis cs: 1,0) -- (reactionStart);
            \draw[thick, dotted, gray] (axis cs: 2.05, 0) -- (reactionEnd);

            \draw[ultra thick, darkblue, rounded corners] (graphStart) -- (reactionStart) to[out=85,in=200] (peak) to[out=-20,in=90] (reactionEnd) -- (graphEnd);

            \path (graphStart) -- (reactionStart) node [above, midway] {{\small H (Edukte)}};
            \path (reactionEnd) -- (graphEnd) node [above, midway] {{\small H (Produkte)}};
            \node[text centered, above] at (axis cs: 1.45,3) {{\small H (Übergangszustand)}};

            \draw[->, > = stealth, thick, dashed] (axis cs: 2.3, 0.4) -- (axis cs: 2.3, 1.8) node[right, midway] {{\small $\Delta$H}};
            \draw[->, > = stealth, thick, dashed] (axis cs: 1.6, 0.4) -- (axis cs: 1.6, 2.95) node[below, midway, sloped] {{\footnotesize Aktivierungsenergie}};

            \node[below, text centered] at (axis cs: 1,0) {{\small Start}};
            \node[below, text centered] at (axis cs: 2.05,0) {{\small Ende}};
        \end{axis}
    \end{tikzpicture}
    }
    \caption{Enthalpiediagramme exothermer und endothermer Reaktionen}
    \label{fig:Enthalpiediagramme}
\end{figure}

Bei einem \textbf{exothermen Vorgang} entstehen energieärmere Stoffe, das System verliert Energie an die Umgebung. Der Wert der \textbf{Reaktionsenthalpie} ist \textbf{negativ}. \\
Bei einem \textbf{endothermen Vorgang} entstehen energiereiche Stoffe, das System gewinnt Energie aus der Umgebung. Der Wert der \textbf{Reaktionsenthalpie} ist \textbf{positiv}.
\newpage
Die Entropie\footnote{Entropie beschreibt die Ordnung in einem System. Je grösser die Unordnung desto höher die Entropie.\\Beispiel niedrige Entropie: Kaffeetasse steht auf dem Tisch.\\Beispiel hohe Entropie: Kaffeetasse fällt vom Tisch und zerspringt $\rightarrow$ Scherben und Kaffee fliegen in alle Richtungen.} eines Systems nimmt mit der Unordnung zu. Exotherme Vorgänge, bei denen die Entropie zunimmt, sind spontan. Endotherme Vorgänge können spontan sein, wenn die Entropie (stark) zunimmt. Vorgänge, bei denen die Entropie abnimmt, können spontan sein, wenn sie (stark) exotherm verlaufen.

\begin{center}
    \begin{tikzpicture}
        \coordinate (center) at (3,3);
        \coordinate (east) at (6,3);
        \coordinate (south) at (3,0);
        \coordinate (west) at (0,3);
        \coordinate (north) at (3,6);
        \coordinate (northeast) at (4,4);
        \coordinate (southeast) at (4,2);
        \coordinate (southwest) at (2,2);
        \coordinate (northwest) at (2,4);

        \draw[fill=lightgray, lightgray] (3,3) circle (3cm);
        \draw[-> , >=stealth, thick] (center) -- (east);
        \draw[-> , >=stealth, thick] (center) -- (south);
        \draw[-> , >=stealth, thick] (center) -- (west);
        \draw[-> , >=stealth, thick] (center) -- (north);
        \draw[-> , >=stealth] (center) -- (southeast);
        \draw[-> , >=stealth] (center) -- (southwest);
        \draw[-> , >=stealth] (center) -- (northwest);
        \draw[-> , >=stealth] (center) -- (northeast);

        \path (center) -- (east) node [above, midway] {{\small Entropie steigt}};
        \path (center) -- (south) node [above, midway, sloped] {{\small Energie sinkt}};
        \path (center) -- (west) node [above, midway] {{\small Entropie sinkt}};
        \path (center) -- (north) node [above, midway, sloped] {{\small Energie steigt}};

        \node [below right= 0cm and 0cm of southeast, text width = 1.2cm] {{\small immer spontan}};
        \node [below left= 0cm and 0cm of southwest, text width = 1.2cm] {{\small evtl. spontan}};
        \node [above left= 0cm and 0cm of northwest, text width = 1.3cm] {{\small nie spontan}};
        \node [above right= 0cm and 0cm of northeast, text width = 1.2cm] {{\small evtl. spontan}};

        \node [below right= 2cm and -0.6cm of east, text width = 5cm] {{\small Vorgänge, bei denen die Energie ab- und die Entropie zunimmt, verlaufen immer spontan.}};
        \node [below left= 2cm and -1cm of west, text width = 5cm] {{\small Vorgänge, bei denen die Energie und Entropie abnimmt, können spontan verlaufen.}};
        \node [above left= 2cm and -1cm of west, text width = 5cm] {{\small Vorgänge, bei denen die Energie zu- und die Entropie abnimmt, verlaufen nie spontan.}};
        \node [above right= 2cm and -0.6cm of east, text width = 5cm] {{\small Vorgänge, bei denen die Energie und Entropie zunimmt, können spontan verlaufen.}};
    \end{tikzpicture}
\end{center}