\section{Protolysengleichgewichte}
\subsection{Autoprotolyse des Wassers}
Da Wasser ein Ampholyt ist kann es sich \textbf{selber protolysieren}. Diesen Vorgang nennt man die \textit{Autoprotolyse} des Wassers.\\
Wasser-Moleküle, die ein Proton abgeben, werden zu \ch{OH-}-Ionen. Wasser-Moleküle, die ein Proton aufnehmen, werden zu \ch{H3O+}-Ionen. Da die \ch{H3O+}-Ionen Protonen leicht an die \ch{OH-}-Ionen abgeben, stellt sich ein Gleichgewicht ein.\\
\begin{center}
    \ch{"\OX{h1,H}" {}2O + "\OX{h2,H}" {}2O <<=> "\OX{h3,H}" {}3O+ + O "\OX{h4,H}" {}- }
    \redox(h1, h2)[-stealth]{ \ch{H+}}
    \redox(h3, h4)[-stealth]{ \ch{H+}}
\end{center}
Da Wasser als Säure viel schwächer ist als \ch{H3O+} und als Base viel schwächer ist als \ch{OH-} liegt das \textbf{Gleichgewicht} der Autoprotolyse \textbf{extrem stark links}.\\
Die Konzentration der Wasser-Moleküle ist im Verhältnis zu der Konzentration der Ionen so gross, dass sie im Wasser und in verdünnten Lösungen als \text{konstant} betrachtet wird. Um sie darzustellen verwendet man das \textit{Ionenprodukt} des Wassers \Kw.
    \begin{equation*}
        \Kw = c(\ch{H3O+}) \cdot c(\ch{OH-})
    \end{equation*}
In wässrigen Lösungen gilt: $\Kw = \SI{e-14}{\mole^{2}\liter^{2}}$\\
In reinem Wasser gilt (bei $\SI{25}{\degreeCelsius}$): $c(\ch{H3O+}) = c(\ch{OH-}) = \SI{e-7}{\mole\per\liter}$\\
Durch die entstehenden Oxonium- und Hydroxid-Ionen besitzt Wasser eine geringe elektrische Leitfähigkeit.

\newpage 

\subsection{Der \pH-Wert}
Der \pH-Wert gibt die \ch{H3O+}-Konzentration in einer wässrigen Lösung an. Er nimmt mit der Verzehnfachung der \ch{H3O+}-Konzentration um $1$ ab. Die konventionelle \pH-Skala reicht von $0$ bis $14$.
\begin{center}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {c|c|c}
        sauer & neutral & alkalisch\\
        $\pH < 7$ & $\pH = 7$ & $\pH > 7$\\
        $c(\ch{H3O+}) > c(\ch{OH-})$ & $c(\ch{H3O+}) = c(\ch{OH-})$ & $c(\ch{H3O+}) < c(\ch{OH-})$\\
        $1 > c(\ch{H3O+}) > 10^{-7}$ &  $c(\ch{H3O+}) = 10^{-7}$ &  $10^{-7} > c(\ch{H3O+}) > 10^{-14}$\\
        $10^{-14} < c(\ch{OH-}) < 10^{-7}$ &  $c(\ch{OH-}) = 10^{-7}$ &  $10^{-7} < c(\ch{OH-}) < 1$
    \end{tabular}
\end{center}

\subsubsection*{Berechnung des \pH-Werts}
Der \pH-Wert ist der negative Zehner-Logarithmus des Zahlwerts der Oxonium-Ionen-Konzentration.
    \begin{equation*}
        \pH = - \log_{10}\{c(\ch{H3O+})\}
    \end{equation*}

\paragraph*{Beispiele} zur Berechnung des \pH-Werts.\\
Berechnung des \pH-Werts einer Lösung mit $c(\ch{H3O+}) = \SI{0.0003}{\mole\per\liter}$
    \begin{equation*}
        \pH = - \log_{10}0.0003 = 3.52
    \end{equation*}
Berechnung \ch{H3O+}-Konzentration einer Lösung mit \pH~$5.2$
    \begin{align*}
        c(\ch{H3O+}) &= 10^{-\pH}\\
        c(\ch{H3O+}) &= 10^{-5.2} = 6.3 \cdot 10^{-6} \si{\mole\per\liter}
    \end{align*}

\subsubsection*{Konzentration der Hydroxid-Ionen und der \pOH}
Der \pOH-Wert gibt die Konzentration der Hydroxid-Ionen in einer wässrigen Lösung an.
    \begin{equation*}
        \pOH = - log_{10}\{c(\ch{OH-})\}
    \end{equation*}

Für verdünnte wässrige Lösungen gilt: $\pH + \pOH = 14$

\subsection{\pH-Wert-Berechnung}
\subsubsection*{Berechnung des \pH-Werts von Lösungen starker Säuren}
Der \pH-Wert ist primär von der \textbf{Säurekonzentration} abhängig. Je \textbf{konzentrierter} eine Säure ist, desto \textbf{tiefer} ist ihr \textbf{\pH-Wert}. Auch die Säurestärke hat einen Einfluss auf den \pH-Wert. Von zwei äquimolaren Lösungen einprotoniger Säuren hat die Lösung der \textbf{stärkeren Säure} den \textbf{tieferen \pH-Wert}.\\
Starke Säuren protolysieren in verdünnten Lösungen vollständig. Die Konzentration der \ch{H3O+}-Ionen ist nach der Protolyse gleich gross, wie die der Säure zu Beginn der Reaktion.\\
Für verdünnte Lösungen starker Säuren gilt:
    \begin{equation*}
        \pH = - \log_{10}\{c(HA)\}
    \end{equation*}

\subsubsection*{Berechnung des \pH-Werts von Hydroxidlösungen}
Die \ch{OH-}-Konzentration ist von der Salzkonzentration und von der Wertigkeit des Hydroxids abhängig. 
\begin{center}
    \ch{Me(OH)_{\textcolor{darkblue}{n}} -> Me^{\textcolor{darkblue}{n}}+ + \textcolor{darkblue}{n}(OH-)}
\end{center}
Daher muss diese Wertigkeit bei der Berechnung des \pOH-Werts für verdünnte Hydroxidlösungen miteinbezogen werden.
    \begin{equation*}
        \pOH = - \log_{10}\{\textcolor{darkblue}{n} \cdot c[\ch{Me(OH)_{\textcolor{darkblue}{n}}}]\}
    \end{equation*}

\subsection{Säurekonstante als Mass für die Säurestärke}
Um die Stärke verschiedener Säuren zu bestimmen, lässt man sie mit Wasser reagieren. Das resultierende Gleichgewicht lässt sich mit der Säurekonstante \Ka charakterisieren. Die Einheit der Säurekonstante ist \si{\mole\per\liter}.
    \begin{equation*}
        \Ka = \frac{c(\ch{H3O+}) \cdot c(\ch{A-})}{c(\ch{HA})}
    \end{equation*}
Da in verdünnten Lösungen nur ein winziger Teil der Wasser-Moleküle reagiert können diese bei der Berechnung der Säurekonstante vernachlässigt werden.\\
Der Wert der Säurekonstante ist umso \textbf{grösser}, je \textbf{stärker} die Säure ist, da der Anteil der Ionen steigt.\\
Da der Wert der Säurekonstante oft sehr gross oder klein sein kann, wird an ihrer Stelle oft der \textit{Säureexponent} (\pKa) angegeben.
    \begin{equation*}
        \pKa = - log_{10}\{\Ka\}
    \end{equation*}

\subsection{Indikatoren}
Ein Säure-Base-Indikator ist eine schwache Säure (\ch{HIn}), deren korrespondierende Base (\ch{In-}) eine andere Farbe besitzt. Diese Farbänderung lässt sich durch die Abgabe bzw. Aufnahme eines Protons erklären. Die Farbe einer Indikator-Lösung ergibt sich aus dem \textbf{Mengenverhältnis} von \ch{HIn} und \ch{In-}. Dieses verändert sich mit dem \pH-Wert, da das Gleichgewicht (von \ch{HIn} und \ch{In-}) von der Konzentration der \ch{H3O+}- und \ch{OH-}-Ionen abhängig ist.\\
Der Anteil der Indikator-Säure (\ch{HIn}) steigt mit sinkendem \pH-Wert.\\
Am \textbf{Umschlagspunkt} sind die Mengen/Konzentrationen von \textbf{\ch{HIn}} und \textbf{\ch{In-}} \textbf{gleich gross}. Beim Umschlagspunkt besitzt die Indikator-Lösung eine Mischfarbe. Am Umschlagspunkt sind die \textbf{\pH-Werte} und \textbf{\pKa-Werte} von \ch{HIn} \textbf{gleich gross}.\\
Der Farbwechsel der Indikator-Lösung geschieht jedoch nicht schlagartig, sondern zieht sich über einen \pH-Bereich hin. Diesen Bereich nennt man den \textbf{Umschlagsbereich}. Er erstreckt sich über \textbf{zwei \pH-Einheiten} ($\pH = \pKa \pm 1$).\\
Durch die Kombination mehrerer Indikatoren erhält man \textit{Universalindikatoren}. Diese können ihre Farben mehrmals ändern und dienen der Grobbestimmung des \pH-Werts.

\newpage

\subsection{Puffer}
Viele chemische Vorgänge benötigen eine bestimmte \ch{H3O+}-Konzentration um optimal ablaufen zu können. Um den \pH-Wert in einem System, auch bei der Zugabe von Säuren oder Laugen, konstant zu halten braucht man Stoffe, welche die dazukommenden \ch{H3O+}- und \ch{OH-}-Ionen neutralisieren. Solche Stoffe oder Stoffgemische nennt man Puffer. Sie bestehen meist aus einer schwachen Säure in Kombination mit ihrer korrespondierenden Base. Als Puffer-Base verwendet man oft Salze. Gibt man \ch{H3O+}- und \ch{OH-}-Ionen zu einer Pufferlösung dazu, verändert sich zwar die Zusammensetzung des Puffers, aber der \pH-Wert bleibt konstant.\\
\begin{center}
    \textbf{Puffer halten den \pH-Wert einer Lösung auch bei der Zugabe von (beschränkten Mengen) Säuren und Basen konstant.}
\end{center}
Da ein Puffer eine endliche Menge an Teilchen enthält, ist auch die Menge der Ionen die er Puffern kann begrenzt. Seine \textbf{Kapazität} (Menge die er puffern kann) ist von seiner \textbf{Konzentration abhängig}.\\
Der \pH-Wert eines Puffers ist vom \pKa-Wert seines Säure-Base-Paars und dem Mengenverhältnis abhängig. Sind die Konzentrationen der Puffer-Säure und korrespondierender Puffer-Base gleich gross, ist der \pH-Wert der Lösung gleich dem \pKa-Wert der Säure.
    \begin{equation*}
        c(\ch{A-}) = c(\ch{HA}) \qquad \text{gilt} \qquad \pKa = \pH \qquad \text{(und umgekehrt)}
    \end{equation*}
Der \pH-Bereich, in dem ein Säure-Base-Paar optimal puffert, liegt nahe beim \pKa-Wert der Puffer-Säure ($\pH = \pKa \pm 1$).\\
Ist der \pH-Wert einer Lösung gleich dem \pKa-Wert der Säure, sind die Konzentrationen der Säure und deren korrespondierenden Base gleich gross.